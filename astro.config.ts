import { defineConfig } from "astro/config";
import sitemap from "@astrojs/sitemap";

import UnoCSS from "unocss/astro";

export default defineConfig({
  site: "https://alexislours.com",
  outDir: "public",
  publicDir: "static",
  prefetch: true,
  build: {
    inlineStylesheets: "auto",
  },
  image: {
    domains: ["api.mapbox.com"],
  },
  integrations: [
    sitemap(),
    UnoCSS({
      injectReset: true,
    }),
  ],
});
