export const SITE_TITLE = "Alexis LOURS";
export const SITE_DESCRIPTION = "A portfolio of my (mostly wildlife) pictures.";
export const EMAIL = "alexislours@protonmail.com";
export const REPO = "https://gitlab.com/alexislours/alexislours.com";
