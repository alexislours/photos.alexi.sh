---
id: zvdheyjf
slug: photo/zvdheyjf
title: Nocturnal Glow
description: A night shot in bois des Éboulures.
createdAt: 2023-11-29T18:13:00Z
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.994938
  longitude: 2.210337
photo: "./759A5695.jpg"
exif:
  iso: 12800
  aperture: 5.6
  shutterSpeed: "1/8"
  focalLength: 35
  camera: Canon EOS R7
  lens: Canon RF 35mm F1.8 MACRO IS STM
---
