---
id: kkbfswue
slug: photo/kkbfswue
title: Étang de Bergerie, Champcevrais
description: A little fishing hut on the shore of the étang de bergerie in Champcevrais, France.
createdAt: 2021-05-30T14:07:00Z
geolocation:
  name: Étang de Bergerie, Champcevrais, France
  latitude: 47.742691
  longitude: 2.939622
photo: "./E7D20991.jpg"
exif:
  iso: 200
  aperture: 4.5
  shutterSpeed: "1/1600"
  focalLength: 50
  camera: Canon EOS 7D Mark II
  lens: Canon EF 50mm f/1.8 STM
---
