---
id: ytsectvn
slug: photo/ytsectvn
title: Cotinus coggygria
description: Close-up of a Cotinus coggygria flower, also known as the purple smoke tree.
createdAt: 2021-05-20T12:33:00Z
geolocation:
  name: Franconville, France
  latitude: 48.997168
  longitude: 2.219907
photo: "./E7D29767.jpg"
exif:
  iso: 200
  aperture: 2
  shutterSpeed: "1/1250"
  focalLength: 50
  camera: Canon EOS 7D Mark II
  lens: Canon EF 50mm f/1.8 STM
---
