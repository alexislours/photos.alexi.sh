import { defineCollection, z } from "astro:content";

const galleries = defineCollection({
  schema: ({ image }) =>
    z.object({
      id: z.string(),
      title: z.string(),
      cover: image().refine((img) => img.width >= 1080, {
        message: "Cover image must be at least 1080 pixels wide!",
      }),
      priority: z.number(),
    }),
});

const animalPicture = defineCollection({
  schema: ({ image }) =>
    z.object({
      id: z.string(),
      title: z.string(),
      description: z.string(),
      isLarge: z.boolean().optional(),
      createdAt: z.date(),
      commonName: z.string(),
      scientificName: z.string(),
      family: z.string(),
      geolocation: z.object({
        name: z.string(),
        latitude: z.number(),
        longitude: z.number(),
      }),
      photo: image().refine((img) => img.width >= 1080, {
        message: "Cover image must be at least 1080 pixels wide!",
      }),
      exif: z.object({
        iso: z.number().optional(),
        aperture: z.number().optional(),
        shutterSpeed: z.string().optional(),
        focalLength: z.number().optional(),
        camera: z.string().optional(),
        lens: z.string().optional(),
      }),
    }),
});

const regularPicture = defineCollection({
  schema: ({ image }) =>
    z.object({
      id: z.string(),
      title: z.string(),
      description: z.string(),
      isLarge: z.boolean().optional(),
      createdAt: z.date(),
      geolocation: z.object({
        name: z.string(),
        latitude: z.number(),
        longitude: z.number(),
      }),
      photo: image().refine((img) => img.width >= 1080, {
        message: "Cover image must be at least 1080 pixels wide!",
      }),
      exif: z.object({
        iso: z.number().optional(),
        aperture: z.number().optional(),
        shutterSpeed: z.string().optional(),
        focalLength: z.number().optional(),
        camera: z.string().optional(),
        lens: z.string().optional(),
      }),
    }),
});

const birds = animalPicture;

const arachnids = animalPicture;

const mammals = animalPicture;

const misc = regularPicture;

export const collections = { birds, arachnids, misc, mammals, galleries };
