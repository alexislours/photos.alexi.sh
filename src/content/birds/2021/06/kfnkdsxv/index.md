---
id: kfnkdsxv
slug: photo/kfnkdsxv
title: Carrion Crow
description: A Carrion Crow in Gennevilliers, France.
createdAt: 2021-06-26T12:00:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.941180
  longitude: 2.320211
photo: "./E7D24818.jpg"
exif:
  iso: 1600
  aperture: 5.6
  shutterSpeed: "1/1000"
  focalLength: 300
  camera: Canon EOS 7D Mark II
  lens: TAMRON SP 70-300mm F/4-5.6 Di VC USD
---
