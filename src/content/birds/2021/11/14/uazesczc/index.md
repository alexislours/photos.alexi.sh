---
id: uazesczc
slug: photo/uazesczc
title: Yellowhammer
description: A Yellowhammer in Bouffémont, France.
createdAt: 2021-11-14T15:18:00Z
commonName: Yellowhammer
scientificName: Emberiza citrinella
family: Emberizidae
geolocation:
  name: Bouffémont, France
  latitude: 49.050115
  longitude: 2.306968
photo: "./E7D26806.jpg"
exif:
  iso: 200
  aperture: 6.3
  shutterSpeed: "1/500"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
