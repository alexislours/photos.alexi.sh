---
id: wwudfmqv
slug: photo/wwudfmqv
title: Common Kingfisher
description: A Common Kingfisher in Gennevilliers, France.
createdAt: 2021-10-16T11:55:00Z
commonName: Common Kingfisher
scientificName: Alcedo atthis
family: Alcedinidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934896
  longitude: 2.314269
photo: "./E7D25900.jpg"
exif:
  iso: 320
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 562
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
