---
id: ewnvphus
slug: photo/ewnvphus
title: Grey Heron
description: A Grey Heron in Paris, France.
createdAt: 2021-08-14T13:22:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Bois de Boulogne, Paris, France
  latitude: 48.867725
  longitude: 2.265194
photo: "./E7D28104.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/2000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
