---
id: nzybsdsg
slug: photo/nzybsdsg
title: Common Tern
description: A Common Tern in Gennevilliers, France.
isLarge: true
createdAt: 2021-08-10T07:53:00Z
commonName: Common Tern
scientificName: Sterna hirundo
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934898
  longitude: 2.314223
photo: "./E7D27476.jpg"
exif:
  iso: 2000
  aperture: 6.3
  shutterSpeed: "1/1250"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
