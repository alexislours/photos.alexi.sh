---
id: fjdygymp
slug: photo/fjdygymp
title: European Robin
description: An European Robin in Franconville, France.
createdAt: 2021-08-10T13:54:00Z
commonName: European Robin
scientificName: Erithacus rubecula
family: Muscicapidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 49.004305
  longitude: 2.217858
photo: "./E7D27558.jpg"
exif:
  iso: 3200
  aperture: 6.3
  shutterSpeed: "1/2000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
