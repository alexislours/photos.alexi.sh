---
id: pcpguhap
slug: photo/pcpguhap
title: Long-tailed Tit
description: A Long-tailed Tit in Gennevilliers, France.
createdAt: 2021-08-28T09:59:00Z
commonName: Long-tailed Tit
scientificName: Aegithalos caudatus
family: Aegithalidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934401
  longitude: 2.315052
photo: "./E7D20394.jpg"
exif:
  iso: 2000
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
