---
id: mggsjhsy
slug: photo/mggsjhsy
title: Spotted Flycatcher
description: A Spotted Flycatcher in Gennevilliers, France.
isLarge: true
createdAt: 2021-08-22T15:10:00Z
commonName: Spotted Flycatcher
scientificName: Muscicapa striata
family: Muscicapidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.934818
  longitude: 2.317390
photo: "./E7D29654.jpg"
exif:
  iso: 2500
  aperture: 6.3
  shutterSpeed: "1/1250"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
