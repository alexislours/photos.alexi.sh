---
id: cmfrhhnd
slug: photo/cmfrhhnd
title: Common Tern
description: A Common Tern in flight in Gennevilliers, France.
createdAt: 2021-07-05T14:48:00Z
commonName: Common Tern
scientificName: Sterna hirundo
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934852
  longitude: 2.314682
photo: "./E7D25072.jpg"
exif:
  iso: 2000
  aperture: 5.6
  shutterSpeed: "1/5000"
  focalLength: 300
  camera: Canon EOS 7D Mark II
  lens: TAMRON SP 70-300mm F/4-5.6 Di VC USD
---
