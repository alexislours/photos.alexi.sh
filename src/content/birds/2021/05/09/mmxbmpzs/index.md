---
id: mmxbmpzs
slug: photo/mmxbmpzs
title: Canada Goose
description: A Canada Gosling in Baillet-en-France, France.
createdAt: 2021-05-09T08:46:00Z
commonName: Canada Goose
scientificName: Branta canadensis
family: Anatidae
geolocation:
  name: Parc de Baillet, Baillet-en-France, France
  latitude: 49.065006
  longitude: 2.281967
photo: "./E7D27894.jpg"
exif:
  iso: 2500
  aperture: 5
  shutterSpeed: "1/1600"
  focalLength: 200
  camera: Canon EOS 7D Mark II
  lens: TAMRON SP 70-300mm F/4-5.6 Di VC USD
---
