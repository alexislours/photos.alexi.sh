---
id: vjagfzws
slug: photo/vjagfzws
title: Mandarin Duck
description: A Mandarin Duck in Franconville, France.
isLarge: true
createdAt: 2021-12-10T15:31:00Z
commonName: Mandarin Duck
scientificName: Aix galericulata
family: Anatidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 49.004305
  longitude: 2.217858
photo: "./E7D27548.jpg"
exif:
  iso: 1000
  aperture: 6.3
  shutterSpeed: "1/400"
  focalLength: 531
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
