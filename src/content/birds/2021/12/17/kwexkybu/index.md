---
id: kwexkybu
slug: photo/kwexkybu
title: Eurasian Wren
description: An Eurasian Wren in Franconville, France.
createdAt: 2021-12-17T15:10:00Z
commonName: Eurasian Wren
scientificName: Troglodytes troglodytes
family: Troglodytidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.993602
  longitude: 2.212026
photo: "./E7D27815.jpg"
exif:
  iso: 1000
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
