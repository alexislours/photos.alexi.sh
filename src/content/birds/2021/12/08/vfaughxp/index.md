---
id: vfaughxp
slug: photo/vfaughxp
title: Yellow-legged Gull
description: A juvenile Yellow-legged Gull in flight in Gennevilliers, France.
createdAt: 2021-12-08T14:21:00Z
commonName: Yellow-legged Gull
scientificName: Larus michahellis
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934870
  longitude: 2.314359
photo: "./E7D27343.jpg"
exif:
  iso: 500
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
