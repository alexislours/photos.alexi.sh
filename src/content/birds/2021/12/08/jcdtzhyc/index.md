---
id: jcdtzhyc
slug: photo/jcdtzhyc
title: Common Moorhen
description: A Common Moorhen in Gennevilliers, France.
createdAt: 2021-12-08T11:44:00Z
commonName: Common Moorhen
scientificName: Gallinula chloropus
family: Rallidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934870
  longitude: 2.314359
photo: "./E7D27211.jpg"
exif:
  iso: 1000
  aperture: 6.3
  shutterSpeed: "1/1600"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
