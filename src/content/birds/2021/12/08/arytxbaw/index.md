---
id: arytxbaw
slug: photo/arytxbaw
title: Common Kingfisher
description: A Common Kingfisher in Gennevilliers, France.
createdAt: 2021-12-08T14:27:00Z
commonName: Common Kingfisher
scientificName: Alcedo atthis
family: Alcedinidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934870
  longitude: 2.314359
photo: "./E7D27379.jpg"
exif:
  iso: 640
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
