---
id: ypynxpef
slug: photo/ypynxpef
title: Eurasian Kestrel
description: A male Eurasian Kestrel in Franconville, France.
createdAt: 2024-03-01T15:23:00Z
commonName: Eurasian Kestrel
scientificName: Falco tinnunculus
family: Falconidae
geolocation:
  name: Franconville, France
  latitude: 49.004293
  longitude: 2.226153
photo: "./759A5767.jpg"
exif:
  iso: 3200
  aperture: 8
  shutterSpeed: "1/2500"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
