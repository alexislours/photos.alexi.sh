---
id: wnzvbpkf
slug: photo/wnzvbpkf
title: Eurasian Kestrel
description: An Eurasian Kestrel in Franconville, France.
isLarge: true
createdAt: 2024-01-01T14:18:30Z
commonName: Eurasian Kestrel
scientificName: Falco tinnunculus
family: Falconidae
geolocation:
  name: Bois de Boissy, Franconville, France
  latitude: 49.003747
  longitude: 2.222945
photo: "./759A8060.jpg"
exif:
  iso: 1600
  aperture: 8
  shutterSpeed: "1/2500"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
