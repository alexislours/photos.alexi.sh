---
id: dacrueaw
slug: photo/dacrueaw
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
createdAt: 2024-01-07T10:27:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.952440
  longitude: 2.402466
photo: "./759A8415.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
