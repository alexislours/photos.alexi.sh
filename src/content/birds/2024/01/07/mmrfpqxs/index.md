---
id: mmrfpqxs
slug: photo/mmrfpqxs
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
createdAt: 2024-01-07T10:49:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.952440
  longitude: 2.402466
photo: "./759A8492.jpg"
exif:
  iso: 1250
  aperture: 8
  shutterSpeed: "1/160"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
