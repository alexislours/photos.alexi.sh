---
id: wncknjek
slug: photo/wncknjek
title: Greylag Goose
description: A Greylag Goose in La Courneuve, France.
createdAt: 2024-01-07T10:00:00Z
commonName: Greylag Goose
scientificName: Anser anser
family: Anatidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.949145
  longitude: 2.401766
photo: "./759A8317.jpg"
exif:
  iso: 3200
  aperture: 8
  shutterSpeed: "1/320"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
