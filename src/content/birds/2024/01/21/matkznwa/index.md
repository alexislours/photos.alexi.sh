---
id: matkznwa
slug: photo/matkznwa
title: Eurasian Robin
description: An Eurasian Robin in Gennevilliers, France.
createdAt: 2024-01-21T12:34:00Z
commonName: Eurasian Robin
scientificName: Erithacus rubecula
family: Muscicapidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934788
  longitude: 2.314310
photo: "./759A9446.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/125"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
