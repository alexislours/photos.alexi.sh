---
id: wbydszem
slug: photo/wbydszem
title: Eurasian Coot
description: An Eurasian Coot in La Courneuve, France.
createdAt: 2024-01-21T11:13:00Z
commonName: Eurasian Coot
scientificName: Fulica atra
family: Rallidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.945867
  longitude: 2.399617
photo: "./759A9319.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
