---
id: ufkstujx
slug: photo/ufkstujx
title: Eurasian Robin
description: An Eurasian Robin in Gennevilliers, France.
createdAt: 2024-01-21T13:19:00Z
commonName: Eurasian Robin
scientificName: Erithacus rubecula
family: Muscicapidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934834
  longitude: 2.314241
photo: "./759A9460.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/125"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
