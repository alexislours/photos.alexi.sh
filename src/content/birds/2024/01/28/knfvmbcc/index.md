---
id: knfvmbcc
slug: photo/knfvmbcc
title: Common Firecrest
description: A Common Firecrest in Bouffémont, France.
createdAt: 2024-01-28T10:33:00Z
commonName: Common Firecrest
scientificName: Regulus ignicapilla
family: Regulidae
geolocation:
  name: Bouffémont, France
  latitude: 49.045463
  longitude: 2.315421
photo: "./759A0449.jpg"
exif:
  iso: 8000
  aperture: 8
  shutterSpeed: "1/800"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
