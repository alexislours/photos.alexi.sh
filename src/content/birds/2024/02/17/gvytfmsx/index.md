---
id: gvytfmsx
slug: photo/gvytfmsx
title: Barnacle Goose
description: A Barnacle Goose in La Courneuve, France.
createdAt: 2024-02-17T09:03:00Z
commonName: Barnacle Goose
scientificName: Branta leucopsis
family: Anatidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948968
  longitude: 2.399614
photo: "./759A4055.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/500"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
