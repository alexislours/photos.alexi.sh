---
id: pxbmapvt
slug: photo/pxbmapvt
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
isLarge: true
createdAt: 2024-02-17T10:09:00Z
commonName: Barnacle Goose
scientificName: Branta leucopsis
family: Anatidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948302
  longitude: 2.400702
photo: "./759A4289.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/800"
  focalLength: 359
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
