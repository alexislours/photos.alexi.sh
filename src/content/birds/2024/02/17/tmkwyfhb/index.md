---
id: tmkwyfhb
slug: photo/tmkwyfhb
title: Tufted Duck
description: Tufted Ducks in La Courneuve, France.
createdAt: 2024-02-17T11:59:00Z
commonName: Tufted Duck
scientificName: Aythya fuligula
family: Anatidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948335
  longitude: 2.401016
photo: "./759A4674.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
