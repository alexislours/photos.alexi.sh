---
id: acczbwdg
slug: photo/acczbwdg
title: Rose-ringed Parakeet
description: A Rose-ringed Parakeet in Franconville, France.
createdAt: 2024-02-02T14:05:00Z
commonName: Rose-ringed Parakeet
scientificName: Psittacula krameri
family: Psittacidae
geolocation:
  name: Franconville, France
  latitude: 48.997189
  longitude: 2.214381
photo: "./759A1300.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/800"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
