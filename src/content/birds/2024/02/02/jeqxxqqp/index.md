---
id: jeqxxqqp
slug: photo/jeqxxqqp
title: Eurasian Kestrel
description: An Eurasian Kestrel in Franconville, France.
createdAt: 2024-02-02T12:36:00Z
commonName: Eurasian Kestrel
scientificName: Falco tinnunculus
family: Falconidae
geolocation:
  name: Franconville, France
  latitude: 49.006154
  longitude: 2.224758
photo: "./759A1175.jpg"
exif:
  iso: 640
  aperture: 8
  shutterSpeed: "1/2000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
