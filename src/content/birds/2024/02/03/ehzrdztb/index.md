---
id: ehzrdztb
slug: photo/ehzrdztb
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
createdAt: 2024-02-03T09:43:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948160
  longitude: 2.400224
photo: "./759A1527.jpg"
exif:
  iso: 2500
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 248
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
