---
id: wgkknhrb
slug: photo/wgkknhrb
title: Western Cattle Egret
description: A Western Cattle Egret in Gennevilliers, France.
isLarge: true
createdAt: 2024-02-03T14:18:00Z
commonName: Western Cattle Egret
scientificName: Bubulcus ibis
family: Ardeidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.937248
  longitude: 2.314678
photo: "./759A2546.jpg"
exif:
  iso: 2500
  aperture: 8
  shutterSpeed: "1/1000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
