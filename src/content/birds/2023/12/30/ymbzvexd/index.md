---
id: ymbzvexd
slug: photo/ymbzvexd
title: Long-tailed Tit
description: A Long-tailed Tit in Franconville, France.
isLarge: true
createdAt: 2023-12-30T12:07:00Z
commonName: Long-tailed Tit
scientificName: Aegithalos caudatus
family: Aegithalidae
geolocation:
  name: Franconville, France
  latitude: 48.996959
  longitude: 2.218158
photo: "./759A7790.jpg"
exif:
  iso: 800
  aperture: 8
  shutterSpeed: "1/200"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
