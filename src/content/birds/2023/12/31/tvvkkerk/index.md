---
id: tvvkkerk
slug: photo/tvvkkerk
title: Eurasian Sparrowhawk
description: An Eurasian Sparrowhawk in Franconville, France.
createdAt: 2023-12-31T13:39:30Z
commonName: Eurasian Sparrowhawk
scientificName: Accipiter nisus
family: Accipitridae
geolocation:
  name: Bois de Boissy, Franconville, France
  latitude: 49.007075
  longitude: 2.219923
photo: "./759A7944.jpg"
exif:
  iso: 320
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
