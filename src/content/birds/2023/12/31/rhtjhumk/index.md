---
id: rhtjhumk
slug: photo/rhtjhumk
title: Common Moorhen
description: A Common Moorhen in Franconville, France.
createdAt: 2023-12-31T12:26:00Z
commonName: Common Moorhen
scientificName: Gallinula chloropus
family: Rallidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.993858
  longitude: 2.211608
photo: "./759A7870.jpg"
exif:
  iso: 250
  aperture: 8
  shutterSpeed: "1/400"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
