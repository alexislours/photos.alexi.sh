---
id: ezmfdaev
slug: photo/ezmfdaev
title: Eurasian Wren
description: An Eurasian Wren in Franconville, France.
isLarge: true
createdAt: 2023-12-31T11:42:00Z
commonName: Eurasian Wren
scientificName: Troglodytes troglodytes
family: Troglodytidae
geolocation:
  name: Franconville, France
  latitude: 48.996248
  longitude: 2.213327
photo: "./759A7822.jpg"
exif:
  iso: 3200
  aperture: 8
  shutterSpeed: "1/250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
