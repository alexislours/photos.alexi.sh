---
id: gewbedfq
slug: photo/gewbedfq
title: Eurasian Blue Tit
description: An Eurasian Blue Tit in Franconville, France.
createdAt: 2023-12-31T12:52:00Z
commonName: Eurasian Blue Tit
scientificName: Cyanistes caeruleus
family: Paridae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.997033
  longitude: 2.214240
photo: "./759A7903.jpg"
exif:
  iso: 640
  aperture: 8
  shutterSpeed: "1/400"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
