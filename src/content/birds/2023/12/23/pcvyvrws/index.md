---
id: pcvyvrws
slug: photo/pcvyvrws
title: Carrion Crow
description: A Carrion Crow in La Courneuve, France.
createdAt: 2023-12-23T09:32:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.949057
  longitude: 2.401733
photo: "./759A7116.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/20"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
