---
id: seygwrgc
slug: photo/seygwrgc
title: Black-headed Gull
description: A Black-headed Gull in Franconville, France.
createdAt: 2023-12-13T12:55:00Z
commonName: Black-headed Gull
scientificName: Chroicocephalus ridibundus
family: Laridae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.994081
  longitude: 2.211692
photo: "./759A6461.jpg"
exif:
  iso: 640
  aperture: 8
  shutterSpeed: "1/125"
  focalLength: 259
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
