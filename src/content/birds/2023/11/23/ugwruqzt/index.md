---
id: ugwruqzt
slug: photo/ugwruqzt
title: Common Wood-Pigeon
description: A Common Wood-Pigeon in Enghien, France.
createdAt: 2023-11-23T13:39:00Z
commonName: Common Wood-Pigeon
scientificName: Columba palumbus
family: Columbidae
geolocation:
  name: Lac d'Enghien, Enghien, France
  latitude: 48.968923
  longitude: 2.303884
photo: "./759A4666.jpg"
exif:
  iso: 3200
  aperture: 8
  shutterSpeed: "1/500"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
