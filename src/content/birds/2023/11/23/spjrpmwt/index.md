---
id: spjrpmwt
slug: photo/spjrpmwt
title: Common Moorhen
description: A Common Moorhen in Enghien, France.
createdAt: 2023-11-23T13:28:00Z
commonName: Common Moorhen
scientificName: Gallinula chloropus
family: Rallidae
geolocation:
  name: Lac d'Enghien, Enghien, France
  latitude: 48.968786
  longitude: 2.303937
photo: "./759A4610.jpg"
exif:
  iso: 5000
  aperture: 8
  shutterSpeed: "1/500"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
