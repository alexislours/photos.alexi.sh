---
id: jqfeztcy
slug: photo/jqfeztcy
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
createdAt: 2023-11-25T11:27:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948318
  longitude: 2.400678
photo: "./759A5105.jpg"
exif:
  iso: 500
  aperture: 8
  shutterSpeed: "1/1000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
