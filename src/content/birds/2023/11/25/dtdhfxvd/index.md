---
id: dtdhfxvd
slug: photo/dtdhfxvd
title: Eurasian Coot
description: An Eurasian Coot in La Courneuve, France.
createdAt: 2023-11-25T11:00:00Z
commonName: Eurasian Coot
scientificName: Fulica atra
family: Rallidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.944704
  longitude: 2.394247
photo: "./759A4991.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/1000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
