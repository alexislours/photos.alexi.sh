---
id: nnvyvbak
slug: photo/nnvyvbak
title: Carrion Crow
description: A Carrion Crow in La Courneuve, France.
isLarge: true
createdAt: 2023-11-25T11:55:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.949045
  longitude: 2.401823
photo: "./759A5315.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
