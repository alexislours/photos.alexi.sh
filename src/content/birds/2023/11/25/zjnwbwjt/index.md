---
id: zjnwbwjt
slug: photo/zjnwbwjt
title: Black-headed Gull
description: A Black-headed Gull in La Courneuve, France.
createdAt: 2023-11-25T12:04:00Z
commonName: Black-headed Gull
scientificName: Chroicocephalus ridibundus
family: Laridae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.949208
  longitude: 2.401444
photo: "./759A5340.jpg"
exif:
  iso: 200
  aperture: 8
  shutterSpeed: "1/800"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
