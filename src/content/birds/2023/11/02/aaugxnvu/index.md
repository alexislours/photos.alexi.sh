---
id: aaugxnvu
slug: photo/aaugxnvu
title: Common Ringed Plover
description: A common ringed plover in Saint-Jean-de-Monts, France.
createdAt: 2023-11-02T16:16:00Z
commonName: Common Ringed Plover
scientificName: Charadrius hiaticula
family: Charadriidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.788561
  longitude: -2.085172
photo: "./759A8565.jpg"
exif:
  iso: 3200
  aperture: 8
  shutterSpeed: "1/800"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
