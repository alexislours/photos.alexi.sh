---
id: dfusygnz
slug: photo/dfusygnz
title: Ruddy Turnstone
description: A ruddy turnstone in Saint-Jean-de-Monts, France.
isLarge: true
createdAt: 2023-11-02T15:39:00Z
commonName: Ruddy Turnstone
scientificName: Arenaria interpres
family: Scolopacidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.788561
  longitude: -2.085172
photo: "./759A7525.jpg"
exif:
  iso: 1600
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
