---
id: vjwjykmz
slug: photo/vjwjykmz
title: Tufted Duck
description: A Tufted Duck in La Courneuve, France.
createdAt: 2023-11-26T10:20:00Z
commonName: Tufted Duck
scientificName: Aythya fuligula
family: Anatidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.944691
  longitude: 2.394280
photo: "./759A5491.jpg"
exif:
  iso: 1600
  aperture: 8
  shutterSpeed: "1/800"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
