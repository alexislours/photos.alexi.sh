---
id: yuwcwhcy
slug: photo/yuwcwhcy
title: Eurasian Coot
description: An Eurasian Coot in La Courneuve, France.
createdAt: 2023-11-26T12:49:00Z
commonName: Eurasian Coot
scientificName: Fulica atra
family: Rallidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.944657
  longitude: 2.394056
photo: "./759A5552.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 373
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
