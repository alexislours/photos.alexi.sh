---
id: zzykuqcx
slug: photo/zzykuqcx
title: Black-headed Gull
description: A Black-headed Gull in flight in Gennevilliers, France.
createdAt: 2023-11-26T10:20:00Z
commonName: Black-headed Gull
scientificName: Chroicocephalus ridibundus
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.935168
  longitude: 2.314169
photo: "./759A5488.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
