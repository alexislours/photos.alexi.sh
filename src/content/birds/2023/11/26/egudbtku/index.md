---
id: egudbtku
slug: photo/egudbtku
title: Black-headed Gull
description: A Black-headed Gull in Gennevilliers, France.
createdAt: 2023-11-26T09:27:00Z
commonName: Black-headed Gull
scientificName: Chroicocephalus ridibundus
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934898
  longitude: 2.314268
photo: "./759A5436.jpg"
exif:
  iso: 1600
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
