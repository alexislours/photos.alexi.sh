---
id: gfwnvtxn
slug: photo/gfwnvtxn
title: Common Moorhen
description: A Common Moorhen in La Courneuve, France.
isLarge: true
createdAt: 2023-11-26T11:48:00Z
commonName: Common Moorhen
scientificName: Gallinula chloropus
family: Rallidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.945259
  longitude: 2.395718
photo: "./759A5523.jpg"
exif:
  iso: 4000
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 347
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
