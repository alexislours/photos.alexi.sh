---
id: thhxpdfh
slug: photo/thhxpdfh
title: Common Moorhen
description: Portrait of a Common Moorhen in Gennevilliers, France.
isLarge: true
createdAt: 2023-11-11T13:01:00Z
commonName: Common Moorhen
scientificName: Gallinula chloropus
family: Rallidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934799
  longitude: 2.314294
photo: "./759A3056.jpg"
exif:
  iso: 2500
  aperture: 8
  shutterSpeed: "1/1000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
