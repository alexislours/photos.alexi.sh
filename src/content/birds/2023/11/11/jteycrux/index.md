---
id: jteycrux
slug: photo/jteycrux
title: Great Cormorant
description: A Great Cormorant perched on a branch in La Courneuve, France.
createdAt: 2023-11-11T08:56:00Z
commonName: Great Cormorant
scientificName: Phalacrocorax carbo
family: Phalacrocoracidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948141
  longitude: 2.400228
photo: "./759A2931.jpg"
exif:
  iso: 1600
  aperture: 8
  shutterSpeed: "1/100"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
