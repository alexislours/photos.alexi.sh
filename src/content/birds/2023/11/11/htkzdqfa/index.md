---
id: htkzdqfa
slug: photo/htkzdqfa
title: European Starling
description: European Starlings perched on a windpump in La Courneuve, France.
createdAt: 2023-11-11T08:30:00Z
commonName: European Starling
scientificName: Sturnus vulgaris
family: Sturnidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.943373
  longitude: 2.403236
photo: "./759A2864.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/400"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
