---
id: umpxzrhs
slug: photo/umpxzrhs
title: Eurasian Magpie
description: Portrait of an Eurasian Magpie in Franconvile, France.
createdAt: 2023-11-08T12:42:00Z
commonName: Eurasian Magpie
scientificName: Pica pica
family: Corvidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.994141
  longitude: 2.211207
photo: "./759A2638.jpg"
exif:
  iso: 8000
  aperture: 8
  shutterSpeed: "1/640"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
