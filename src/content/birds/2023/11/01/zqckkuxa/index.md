---
id: zqckkuxa
slug: photo/zqckkuxa
title: Ruddy Turnstone
description: A Ruddy Turnstone in Saint-Jean-de-Monts, France.
createdAt: 2023-11-01T14:50:00Z
commonName: Ruddy Turnstone
scientificName: Arenaria interpres
family: Scolopacidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.785575
  longitude: -2.080786
photo: "./759A5818.jpg"
exif:
  iso: 800
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
