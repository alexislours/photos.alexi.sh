---
id: aefbefgf
slug: photo/aefbefgf
title: Sanderling
description: A Sanderling in Saint-Jean-de-Monts, France.
createdAt: 2023-11-01T16:00:00Z
commonName: Sanderling
scientificName: Calidris alba
family: Scolopacidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.785575
  longitude: -2.080786
photo: "./759A6456.jpg"
exif:
  iso: 2000
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
