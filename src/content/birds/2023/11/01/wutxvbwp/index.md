---
id: wutxvbwp
slug: photo/wutxvbwp
title: Lesser Black-backed Gull
description: A juvenile Lesser Black-backed Gull in Saint-Jean-de-Monts, France.
createdAt: 2023-11-01T15:28:00Z
commonName: Lesser Black-backed Gull
scientificName: Larus fuscus
family: Laridae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.787528
  longitude: -2.085312
photo: "./759A5453.jpg"
exif:
  iso: 400
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
