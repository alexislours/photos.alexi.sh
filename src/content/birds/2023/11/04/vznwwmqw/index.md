---
id: vznwwmqw
slug: photo/vznwwmqw
title: European Stonechat
description: A male European Stonechat in Saint-Jean-de-Monts, France.
createdAt: 2023-11-04T10:43:00Z
commonName: European Stonechat
scientificName: Saxicola rubicola
family: Muscicapidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.788749
  longitude: -2.085068
photo: "./759A9495.jpg"
exif:
  iso: 1250
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
