---
id: ewcjezrc
slug: photo/ewcjezrc
title: Northern Gannet
description: A Northern Gannet in Saint-Jean-de-Monts, France.
createdAt: 2023-11-04T14:08:00Z
commonName: Northern Gannet
scientificName: Morus bassanus
family: Sulidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.787528
  longitude: -2.085312
photo: "./759A0510.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/2000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
