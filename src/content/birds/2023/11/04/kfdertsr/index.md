---
id: kfdertsr
slug: photo/kfdertsr
title: European Stonechat
description: A female European Stonechat in Saint-Jean-de-Monts, France.
createdAt: 2023-11-04T11:00:00Z
commonName: European Stonechat
scientificName: Saxicola rubicola
family: Muscicapidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.788749
  longitude: -2.085068
photo: "./759A9733.jpg"
exif:
  iso: 5000
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
