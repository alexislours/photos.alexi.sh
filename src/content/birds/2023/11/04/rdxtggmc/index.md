---
id: rdxtggmc
slug: photo/rdxtggmc
title: Northern Gannet
description: A Northern Gannet in Saint-Jean-de-Monts, France.
isLarge: true
createdAt: 2023-11-04T14:08:00Z
commonName: Northern Gannet
scientificName: Morus bassanus
family: Sulidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.786493
  longitude: -2.082642
photo: "./759A0526.jpg"
exif:
  iso: 800
  aperture: 8
  shutterSpeed: "1/2000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
