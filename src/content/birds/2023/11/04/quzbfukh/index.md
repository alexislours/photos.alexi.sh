---
id: quzbfukh
slug: photo/quzbfukh
title: Leach's Storm-Petrel
description: A Leach's Storm-Petrel in Saint-Jean-de-Monts, France.
isLarge: true
createdAt: 2023-11-04T15:06:00Z
commonName: Leach's Storm-Petrel
scientificName: Hydrobates leucorhous
family: Hydrobatidae
geolocation:
  name: Saint-Jean-de-Monts, France
  latitude: 46.785575
  longitude: -2.080786
photo: "./759A1815.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/2500"
  focalLength: 248
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
