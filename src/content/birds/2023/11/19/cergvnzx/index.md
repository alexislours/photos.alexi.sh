---
id: cergvnzx
slug: photo/cergvnzx
title: Black-headed Gull
description: A Black-headed Gull in flight in La Courneuve, France.
createdAt: 2023-11-19T10:33:00Z
commonName: Black-headed Gull
scientificName: Chroicocephalus ridibundus
family: Laridae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.945918
  longitude: 2.399207
photo: "./759A3663.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/2000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
