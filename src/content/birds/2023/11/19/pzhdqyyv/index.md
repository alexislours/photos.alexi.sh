---
id: pzhdqyyv
slug: photo/pzhdqyyv
title: Herring Gull
description: An Herring Gull in flight in Gennevilliers, France.
isLarge: true
createdAt: 2023-11-19T13:05:00Z
commonName: Herring Gull
scientificName: Larus argentatus
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.935092
  longitude: 2.313951
photo: "./759A3844.jpg"
exif:
  iso: 640
  aperture: 8
  shutterSpeed: "1/3200"
  focalLength: 300
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
