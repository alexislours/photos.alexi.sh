---
id: eyerbwuv
slug: photo/eyerbwuv
title: Carrion Crow
description: A Carrion Crow in La Courneuve, France.
createdAt: 2023-11-19T09:57:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948681
  longitude: 2.401594
photo: "./759A3634.jpg"
exif:
  iso: 3200
  aperture: 8
  shutterSpeed: "1/2000"
  focalLength: 183
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
