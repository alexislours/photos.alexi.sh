---
id: nbabqbsv
slug: photo/nbabqbsv
title: Carrion Crow
description: Portrait of a Carrion Crow in La Courneuve, France.
isLarge: true
createdAt: 2023-11-19T09:57:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948681
  longitude: 2.401594
photo: "./759A3633.jpg"
exif:
  iso: 3200
  aperture: 8
  shutterSpeed: "1/2000"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
