---
id: wcuvjanf
slug: photo/wcuvjanf
title: Rock Dove
description: A Feral Pigeon in La Courneuve, France.
createdAt: 2023-11-19T10:55:00Z
commonName: Rock Dove
scientificName: Columba livia
family: Columbidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.944562
  longitude: 2.394062
photo: "./759A3718.jpg"
exif:
  iso: 800
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 300
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
