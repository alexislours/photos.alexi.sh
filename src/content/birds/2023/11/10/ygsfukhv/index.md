---
id: ygsfukhv
slug: photo/ygsfukhv
title: Common Moorhen
description: A Common Moorhen swimming in Franconville, France.
createdAt: 2023-11-11T12:40:00Z
commonName: Common Moorhen
scientificName: Gallinula chloropus
family: Rallidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.993901
  longitude: 2.211630
photo: "./759A2750.jpg"
exif:
  iso: 2500
  aperture: 8
  shutterSpeed: "1/800"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
