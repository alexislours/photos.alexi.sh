---
id: adhngdfk
slug: photo/adhngdfk
title: Carrion Crow
description: A Carrion Crow in Gennevilliers, France.
createdAt: 2023-10-28T12:21:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.940559
  longitude: 2.320132
photo: "./759A4591.jpg"
exif:
  iso: 12800
  aperture: 8
  shutterSpeed: "1/500"
  focalLength: 359
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
