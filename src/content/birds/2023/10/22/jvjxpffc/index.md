---
id: jvjxpffc
slug: photo/jvjxpffc
title: Eurasian Magpie
description: An Eurasian Magpie in Gennevilliers, France.
createdAt: 2023-10-22T12:30:00Z
commonName: Eurasian Magpie
scientificName: Pica pica
family: Corvidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934915
  longitude: 2.314321
photo: "./759A3983.jpg"
exif:
  iso: 3200
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
