---
id: yynwphkn
slug: photo/yynwphkn
title: Grey Heron
description: A Grey Heron among statues in Franconville, France.
isLarge: true
createdAt: 2023-10-11T14:42:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.994069
  longitude: 2.211663
photo: "./759A2351.jpg"
exif:
  iso: 640
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
