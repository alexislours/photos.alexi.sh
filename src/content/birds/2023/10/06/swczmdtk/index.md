---
id: swczmdtk
slug: photo/swczmdtk
title: Little Grebe
description: A Little Grebe in Gennevilliers, France.
createdAt: 2023-10-06T09:20:00Z
commonName: Little Grebe
scientificName: Tachybaptus ruficollis
family: Podicipedidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934884
  longitude: 2.314057
photo: "./759A0288.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
