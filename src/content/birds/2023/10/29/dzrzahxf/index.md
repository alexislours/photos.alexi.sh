---
id: dzrzahxf
slug: photo/dzrzahxf
title: Mute Swan
description: A juvenile Mute Swan in Lieusaint, France.
createdAt: 2023-10-29T15:18:00Z
commonName: Mute Swan
scientificName: Cygnus olor
family: Anatidae
geolocation:
  name: Lieusaint, France
  latitude: 48.637428
  longitude: 2.570654
photo: "./759A5067.jpg"
exif:
  iso: 400
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
