---
id: vbaeqamy
slug: photo/vbaeqamy
title: Mute Swan
description: A juvenile Mute Swan in Lieusaint, France.
isLarge: true
createdAt: 2023-10-29T15:21:00Z
commonName: Mute Swan
scientificName: Cygnus olor
family: Anatidae
geolocation:
  name: Lieusaint, France
  latitude: 48.637428
  longitude: 2.570654
photo: "./759A5295.jpg"
exif:
  iso: 500
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 259
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
