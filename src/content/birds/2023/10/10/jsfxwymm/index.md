---
id: jsfxwymm
slug: photo/jsfxwymm
title: Little Grebe
description: A Little Grebe in Gennevilliers, France.
createdAt: 2023-10-10T13:06:00Z
commonName: Little Grebe
scientificName: Tachybaptus ruficollis
family: Podicipedidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934845
  longitude: 2.314496
photo: "./759A2187.jpg"
exif:
  iso: 800
  aperture: 8
  shutterSpeed: "1/1000"
  focalLength: 281
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
