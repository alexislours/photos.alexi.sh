---
id: hfmajpjh
slug: photo/hfmajpjh
title: Egyptian Goose
description: Portrait of an Egyptian Goose in La Courneuve, France.
createdAt: 2023-10-09T11:11:00Z
commonName: Egyptian Goose
scientificName: Alopochen aegyptiaca
family: Anatidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.949099
  longitude: 2.401843
photo: "./759A1662.jpg"
exif:
  iso: 800
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 400
  camera: Canon EOS R7
  lens: Canon RF100-400mm F5.6-8 IS USM
---
