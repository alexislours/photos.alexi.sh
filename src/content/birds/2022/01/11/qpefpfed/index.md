---
id: qpefpfed
slug: photo/qpefpfed
title: Carrion Crow
description: A Carrion Crow in Gennevilliers, France.
isLarge: true
createdAt: 2022-01-11T15:04:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934582
  longitude: 2.316483
photo: "./E7D20164.jpg"
exif:
  iso: 3200
  aperture: 6.3
  shutterSpeed: "1/1250"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
