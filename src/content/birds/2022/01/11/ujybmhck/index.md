---
id: ujybmhck
slug: photo/ujybmhck
title: Common Kingfisher
description: A Common Kingfisher in Gennevilliers, France.
createdAt: 2022-01-11T13:11:00Z
commonName: Common Kingfisher
scientificName: Alcedo atthis
family: Alcedinidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934722
  longitude: 2.314022
photo: "./E7D29699.jpg"
exif:
  iso: 3200
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
