---
id: qhpqusas
slug: photo/qhpqusas
title: Eurasian Wren
description: An Eurasian Wren in Franconville, France.
createdAt: 2022-01-02T11:27:00Z
commonName: Eurasian Wren
scientificName: Troglodytes troglodytes
family: Troglodytidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.995646
  longitude: 2.212362
photo: "./E7D28634.jpg"
exif:
  iso: 800
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
