---
id: ywzmryhm
slug: photo/ywzmryhm
title: Common Firecrest
description: A Common Firecrest in Franconville, France.
createdAt: 2022-01-15T16:21:00Z
commonName: Common Firecrest
scientificName: Regulus ignicapilla
family: Regulidae
geolocation:
  name: Bois de Boissy, Franconville, France
  latitude: 49.005586
  longitude: 2.219198
photo: "./E7D20886.jpg"
exif:
  iso: 1250
  aperture: 7.1
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
