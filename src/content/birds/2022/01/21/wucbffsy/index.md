---
id: wucbffsy
slug: photo/wucbffsy
title: Eurasian Green Woodpecker
description: An Eurasian Green Woodpecker in Franconville, France.
createdAt: 2022-01-21T15:01:00Z
commonName: Eurasian Green Woodpecker
scientificName: Picus viridis
family: Picidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.994900
  longitude: 2.212131
photo: "./E7D21257.jpg"
exif:
  iso: 1250
  aperture: 6.3
  shutterSpeed: "1/640"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
