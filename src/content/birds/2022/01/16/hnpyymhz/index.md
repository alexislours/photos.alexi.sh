---
id: hnpyymhz
slug: photo/hnpyymhz
title: Eurasian Kestrel
description: A Eurasian Kestrel in Franconville, France.
createdAt: 2022-01-16T14:57:00Z
commonName: Eurasian Kestrel
scientificName: Falco tinnunculus
family: Falconidae
geolocation:
  name: Bois de Boissy, Franconville, France
  latitude: 48.934401
  longitude: 2.315052
photo: "./E7D20991.jpg"
exif:
  iso: 3200
  aperture: 6.3
  shutterSpeed: "1/640"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
