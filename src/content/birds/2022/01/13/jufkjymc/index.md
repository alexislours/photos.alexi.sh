---
id: jufkjymc
slug: photo/jufkjymc
title: Eurasian Wren
description: An Eurasian Wren in Franconville, France.
createdAt: 2022-01-13T15:53:00Z
commonName: Eurasian Wren
scientificName: Troglodytes troglodytes
family: Troglodytidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.995646
  longitude: 2.212362
photo: "./E7D20324.jpg"
exif:
  iso: 1600
  aperture: 6.3
  shutterSpeed: "1/800"
  focalLength: 516
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
