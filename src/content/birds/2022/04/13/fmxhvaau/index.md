---
id: fmxhvaau
slug: photo/fmxhvaau
title: Long-tailed Tit
description: A Long-tailed Tit in Franconville, France.
createdAt: 2022-04-13T17:08:00Z
commonName: Long-tailed Tit
scientificName: Aegithalos caudatus
family: Aegithalidae
geolocation:
  name: Franconville, France
  latitude: 48.934401
  longitude: 2.315052
photo: "./E7D21205.jpg"
exif:
  iso: 2500
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 562
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
