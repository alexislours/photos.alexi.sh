---
id: babgdpbp
slug: photo/babgdpbp
title: Water Rail
description: A Water Rail in Saint-Quentin-en-Tourmont, France.
createdAt: 2022-04-30T12:23:00Z
commonName: Water Rail
scientificName: Rallus aquaticus
family: Rallidae
geolocation:
  name: Parc du Marquenterre, Saint-Quentin-en-Tourmont, France
  latitude: 50.261801
  longitude: 1.575528
photo: "./E7D23538.jpg"
exif:
  iso: 400
  aperture: 7.1
  shutterSpeed: "1/1600"
  focalLength: 516
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
