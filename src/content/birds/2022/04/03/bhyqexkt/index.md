---
id: bhyqexkt
slug: photo/bhyqexkt
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
isLarge: true
createdAt: 2022-04-03T09:38:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948198
  longitude: 2.400352
photo: "./E7D20237.jpg"
exif:
  iso: 250
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 468
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
