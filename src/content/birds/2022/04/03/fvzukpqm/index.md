---
id: fvzukpqm
slug: photo/fvzukpqm
title: Canada Goose
description: A Canada Goose in flight in Gennevilliers, France.
createdAt: 2022-04-30T11:41:00Z
commonName: Canada Goose
scientificName: Branta canadensis
family: Anatidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934961
  longitude: 2.314099
photo: "./E7D20393.jpg"
exif:
  iso: 640
  aperture: 8
  shutterSpeed: "1/2000"
  focalLength: 361
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
