---
id: cyxzpkce
slug: photo/cyxzpkce
title: Common Tern
description: A Common Tern in Gennevilliers, France.
createdAt: 2022-04-24T10:33:00Z
commonName: Common Tern
scientificName: Sterna hirundo
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934994
  longitude: 2.314160
photo: "./E7D23133.jpg"
exif:
  iso: 640
  aperture: 7.1
  shutterSpeed: "1/2000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
