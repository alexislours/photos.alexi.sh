---
id: vpdnntcy
slug: photo/vpdnntcy
title: Eurasian Kestrel
description: An Eurasian Kestrel in La Courneuve, France.
createdAt: 2022-04-17T10:07:00Z
commonName: Eurasian Kestrel
scientificName: Falco tinnunculus
family: Falconidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.953090
  longitude: 2.402627
photo: "./E7D22148.jpg"
exif:
  iso: 800
  aperture: 7.1
  shutterSpeed: "1/2000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
