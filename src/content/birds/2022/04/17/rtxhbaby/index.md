---
id: rtxhbaby
slug: photo/rtxhbaby
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
createdAt: 2022-04-17T09:11:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948198
  longitude: 2.400352
photo: "./E7D22094.jpg"
exif:
  iso: 400
  aperture: 7.1
  shutterSpeed: "1/2500"
  focalLength: 361
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
