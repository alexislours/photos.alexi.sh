---
id: hhcftwxr
slug: photo/hhcftwxr
title: Common Tern
description: An Common Tern in Gennevilliers, France.
createdAt: 2022-04-17T12:26:00Z
commonName: Common Tern
scientificName: Sterna hirundo
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.935013
  longitude: 2.314243
photo: "./E7D22276.jpg"
exif:
  iso: 640
  aperture: 7.1
  shutterSpeed: "1/3200"
  focalLength: 435
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
