---
id: xahnnjcx
slug: photo/xahnnjcx
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
createdAt: 2022-04-17T09:07:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948198
  longitude: 2.400352
photo: "./E7D22089.jpg"
exif:
  iso: 1250
  aperture: 7.1
  shutterSpeed: "1/2500"
  focalLength: 324
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
