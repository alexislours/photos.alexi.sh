---
id: vvvydbqw
slug: photo/vvvydbqw
title: Rock Dove
description: A Feral Pigeon in flight in Paris, France
createdAt: 2022-04-06T18:47:00Z
commonName: Rock Dove
scientificName: Columba livia
family: Columbidae
geolocation:
  name: Île aux Cygnes, Paris, France
  latitude: 48.851019
  longitude: 2.281110
photo: "./E7D20626.jpg"
exif:
  iso: 2000
  aperture: 7.1
  shutterSpeed: "1/1250"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
