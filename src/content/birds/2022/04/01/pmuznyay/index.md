---
id: pmuznyay
slug: photo/pmuznyay
title: Black-headed Gull
description: A Black-headed Gull in flight in Gennevilliers, France.
createdAt: 2022-04-01T08:33:00Z
commonName: Black-headed Gull
scientificName: Chroicocephalus ridibundus
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934877
  longitude: 2.314769
photo: "./E7D29929.jpg"
exif:
  iso: 2000
  aperture: 6.3
  shutterSpeed: "1/800"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
