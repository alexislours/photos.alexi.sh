---
id: tpehhyax
slug: photo/tpehhyax
title: Eurasian Coot
description: An Eurasian Coot in La Courneuve, France.
createdAt: 2022-05-05T12:48:00Z
commonName: Eurasian Coot
scientificName: Fulica atra
family: Rallidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948991
  longitude: 2.402614
photo: "./E7D24756.jpg"
exif:
  iso: 3200
  aperture: 7.1
  shutterSpeed: "1/1600"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
