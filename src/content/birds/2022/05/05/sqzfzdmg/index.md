---
id: sqzfzdmg
slug: photo/sqzfzdmg
title: Grey Heron
description: A Grey Heron in La Courneuve, France.
createdAt: 2022-05-05T12:28:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948143
  longitude: 2.400385
photo: "./E7D24736.jpg"
exif:
  iso: 1250
  aperture: 7.1
  shutterSpeed: "1/1600"
  focalLength: 302
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
