---
id: jcsqmywh
slug: photo/jcsqmywh
title: Carrion Crow
description: A Carrion Crow in Gennevilliers, France.
isLarge: true
createdAt: 2022-05-05T09:50:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934822
  longitude: 2.314107
photo: "./E7D24577.jpg"
exif:
  iso: 640
  aperture: 7.1
  shutterSpeed: "1/1600"
  focalLength: 211
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
