---
id: ncdbkwkd
slug: photo/ncdbkwkd
title: Tufted Duck
description: A male Tufted Duck in La Courneuve, France.
createdAt: 2022-03-20T08:14:00Z
commonName: Tufted Duck
scientificName: Aythya fuligula
family: Anatidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948256
  longitude: 2.402235
photo: "./E7D28215.jpg"
exif:
  iso: 500
  aperture: 7.1
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
