---
id: jwvvndmj
slug: photo/jwvvndmj
title: Grey Heron
description: Juvenile Grey Herons in La Courneuve, France
createdAt: 2022-03-20T08:06:00Z
commonName: Grey Heron
scientificName: Ardea cinerea
family: Ardeidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948258
  longitude: 2.400678
photo: "./E7D28190.jpg"
exif:
  iso: 800
  aperture: 7.1
  shutterSpeed: "1/1000"
  focalLength: 468
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
