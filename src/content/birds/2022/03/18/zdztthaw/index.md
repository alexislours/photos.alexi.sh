---
id: zdztthaw
slug: photo/zdztthaw
title: Carrion Crow
description: Portrait of a Carrion Crow in Gennevilliers, France.
createdAt: 2022-03-18T14:43:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.935888
  longitude: 2.316752
photo: "./E7D27850.jpg"
exif:
  iso: 800
  aperture: 6.3
  shutterSpeed: "1/1250"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
