---
id: tdwwyapk
slug: photo/tdwwyapk
title: Eurasian Blue Tit
description: An Eurasian Blue Tit in La Courneuve, France.
createdAt: 2022-03-18T11:02:00Z
commonName: Eurasian Blue Tit
scientificName: Cyanistes caeruleus
family: Paridae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.949639
  longitude: 2.401934
photo: "./E7D27457.jpg"
exif:
  iso: 640
  aperture: 6.3
  shutterSpeed: "1/1600"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
