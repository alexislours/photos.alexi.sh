---
id: ucdkmfbz
slug: photo/ucdkmfbz
title: Great Cormorant
description: A Great Cormorant perched on a branch in La Courneuve, France.
createdAt: 2022-03-18T10:45:00Z
commonName: Great Cormorant
scientificName: Phalacrocorax carbo
family: Phalacrocoracidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.948141
  longitude: 2.400228
photo: "./E7D27389.jpg"
exif:
  iso: 320
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
