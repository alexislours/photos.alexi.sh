---
id: jyqunecf
slug: photo/jyqunecf
title: Rock Dove
description: A Feral Pigeon in La Courneuve, France
createdAt: 2022-03-18T12:25:00Z
commonName: Rock Dove
scientificName: Columba livia
family: Columbidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.949053
  longitude: 2.402011
photo: "./E7D27686.jpg"
exif:
  iso: 320
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 388
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
