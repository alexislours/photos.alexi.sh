---
id: vhfawysa
slug: photo/vhfawysa
title: European Goldfinch
description: An European Goldfinch in Franconville, France.
createdAt: 2022-03-05T13:46:00Z
commonName: European Goldfinch
scientificName: Carduelis carduelis
family: Fringillidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.995369
  longitude: 2.213019
photo: "./E7D26124.jpg"
exif:
  iso: 1600
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
