---
id: ghgrjxxt
slug: photo/ghgrjxxt
title: European Robin
description: An European Robin in Franconville, France.
createdAt: 2022-03-05T13:04:00Z
commonName: European Robin
scientificName: Erithacus rubecula
family: Muscicapidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.995369
  longitude: 2.213019
photo: "./E7D26052.jpg"
exif:
  iso: 1600
  aperture: 6.3
  shutterSpeed: "1/1250"
  focalLength: 421
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
