---
id: kmwyhqtb
slug: photo/kmwyhqtb
title: Long-tailed Tit
description: A Long-tailed Tit in Gennevilliers, France.
createdAt: 2022-03-16T11:26:00Z
commonName: Long-tailed Tit
scientificName: Aegithalos caudatus
family: Aegithalidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.935136
  longitude: 2.309979
photo: "./E7D26857.jpg"
exif:
  iso: 2500
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
