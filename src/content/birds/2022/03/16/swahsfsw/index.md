---
id: swahsfsw
slug: photo/swahsfsw
title: Rock Dove
description: A Feral Pigeon in Paris, France.
isLarge: true
createdAt: 2022-03-16T13:26:00Z
commonName: Rock Dove
scientificName: Columba livia
family: Columbidae
geolocation:
  name: L'Île aux Cygnes, Paris, France
  latitude: 48.850501
  longitude: 2.280631
photo: "./E7D26981.jpg"
exif:
  iso: 400
  aperture: 9
  shutterSpeed: "1/500"
  focalLength: 70
  camera: Canon EOS 7D Mark II
  lens: Sigma 70mm F2.8 DG MACRO | Art
---
