---
id: mhkzdkbz
slug: photo/mhkzdkbz
title: European Robin
description: An European Robin in Franconville, France.
createdAt: 2022-03-26T16:46:00Z
commonName: European Robin
scientificName: Erithacus rubecula
family: Muscicapidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.996414
  longitude: 2.213192
photo: "./E7D29671.jpg"
exif:
  iso: 1600
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
