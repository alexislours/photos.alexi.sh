---
id: tbmrqqra
slug: photo/tbmrqqra
title: Black-headed Gull
description: Black-headed Gulls in Gennevilliers, France.
createdAt: 2022-03-23T15:32:00Z
commonName: Black-headed Gull
scientificName: Chroicocephalus ridibundus
family: Laridae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934867
  longitude: 2.314357
photo: "./E7D29170.jpg"
exif:
  iso: 320
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 335
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
