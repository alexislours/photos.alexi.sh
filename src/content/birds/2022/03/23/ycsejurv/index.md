---
id: ycsejurv
slug: photo/ycsejurv
title: Eurasian Magpie
description: An Eurasian Magpie in Gennevilliers, France.
createdAt: 2022-03-23T14:33:00Z
commonName: Eurasian Magpie
scientificName: Pica pica
family: Corvidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934897
  longitude: 2.314270
photo: "./E7D29053.jpg"
exif:
  iso: 500
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 435
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
