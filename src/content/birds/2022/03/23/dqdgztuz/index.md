---
id: dqdgztuz
slug: photo/dqdgztuz
title: Great Cormorant
description: A Great Cormorant in flight in Gennevilliers, France.
createdAt: 2022-03-23T14:31:00Z
commonName: Great Cormorant
scientificName: Phalacrocorax carbo
family: Phalacrocoracidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.935049
  longitude: 2.314300
photo: "./E7D29039.jpg"
exif:
  iso: 640
  aperture: 8
  shutterSpeed: "1/1600"
  focalLength: 516
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
