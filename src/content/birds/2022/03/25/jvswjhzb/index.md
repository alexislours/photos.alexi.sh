---
id: jvswjhzb
slug: photo/jvswjhzb
title: Mute Swan
description: A Mute Swan in flight in Gennevilliers, France.
createdAt: 2022-03-25T13:56:00Z
commonName: Mute Swan
scientificName: Cygnus olor
family: Anatidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.935049
  longitude: 2.314300
photo: "./E7D29372.jpg"
exif:
  iso: 200
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 191
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
