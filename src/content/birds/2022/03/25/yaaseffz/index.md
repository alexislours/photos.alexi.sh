---
id: yaaseffz
slug: photo/yaaseffz
title: Egyptian Goose
description: An Egyptian Gosling in Villepinte, France.
createdAt: 2022-03-25T09:10:00Z
commonName: Egyptian Goose
scientificName: Alopochen aegyptiaca
family: Anatidae
geolocation:
  name: Parc du Sausset, Villepinte, France
  latitude: 48.957774
  longitude: 2.510285
photo: "./E7D29256.jpg"
exif:
  iso: 500
  aperture: 7.1
  shutterSpeed: "1/1250"
  focalLength: 324
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
