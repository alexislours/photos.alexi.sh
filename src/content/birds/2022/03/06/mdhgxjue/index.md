---
id: mdhgxjue
slug: photo/mdhgxjue
title: Ferruginous Duck
description: A Ferruginous Duck in La Courneuve, France.
createdAt: 2022-03-06T12:04:00Z
commonName: Ferruginous Duck
scientificName: Aythya nyroca
family: Anatidae
geolocation:
  name: Parc Georges-Valbon, La Courneuve, France
  latitude: 48.945756
  longitude: 2.399873
photo: "./E7D26331.jpg"
exif:
  iso: 800
  aperture: 7.1
  shutterSpeed: "1/1600"
  focalLength: 484
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
