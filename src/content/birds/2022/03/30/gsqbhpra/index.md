---
id: gsqbhpra
slug: photo/gsqbhpra
title: Eurasian Magpie
description: An Eurasian Magpie under the rain in Gennevilliers, France.
isLarge: true
createdAt: 2022-03-30T09:30:00Z
commonName: Eurasian Magpie
scientificName: Pica pica
family: Corvidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934900
  longitude: 2.314265
photo: "./E7D29788.jpg"
exif:
  iso: 1250
  aperture: 6.3
  shutterSpeed: "1/1000"
  focalLength: 335
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
