---
id: ychuukpu
slug: photo/ychuukpu
title: Purple Heron
description: A Purple Heron in flight in Gennevilliers, France.
createdAt: 2022-03-30T10:13:00Z
commonName: Purple Heron
scientificName: Ardea purpurea
family: Ardeidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.935034
  longitude: 2.314221
photo: "./E7D29817.jpg"
exif:
  iso: 1250
  aperture: 6.3
  shutterSpeed: "1/800"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
