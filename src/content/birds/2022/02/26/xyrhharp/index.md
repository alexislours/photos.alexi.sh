---
id: xyrhharp
slug: photo/xyrhharp
title: European Robin
description: An European Robin in Créteil, France.
isLarge: true
createdAt: 2022-02-26T11:27:00Z
commonName: European Robin
scientificName: Erithacus rubecula
family: Muscicapidae
geolocation:
  name: Lac de Créteil, Créteil, France
  latitude: 48.780713
  longitude: 2.449381
photo: "./E7D24650.jpg"
exif:
  iso: 500
  aperture: 7.1
  shutterSpeed: "1/800"
  focalLength: 435
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
