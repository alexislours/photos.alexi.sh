---
id: ersxpfja
slug: photo/ersxpfja
title: Long-tailed Tit
description: A Long-tailed Tit in Gennevilliers, France.
createdAt: 2022-02-25T16:10:00Z
commonName: Long-tailed Tit
scientificName: Aegithalos caudatus
family: Aegithalidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934375
  longitude: 2.315024
photo: "./E7D24502.jpg"
exif:
  iso: 400
  aperture: 7.1
  shutterSpeed: "1/1600"
  focalLength: 302
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
