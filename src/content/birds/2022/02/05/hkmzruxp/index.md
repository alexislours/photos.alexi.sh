---
id: hkmzruxp
slug: photo/hkmzruxp
title: Long-tailed Tit
description: A Long-tailed Tit in Gennevilliers, France.
createdAt: 2022-02-05T11:25:00Z
commonName: Long-tailed Tit
scientificName: Aegithalos caudatus
family: Aegithalidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.936398
  longitude: 2.315080
photo: "./E7D22812.jpg"
exif:
  iso: 1000
  aperture: 6.3
  shutterSpeed: "1/1250"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
