---
id: qmxkjwag
slug: photo/qmxkjwag
title: Carrion Crow
description: A Carrion Crow in Franconville, France.
createdAt: 2022-02-08T14:23:00Z
commonName: Carrion Crow
scientificName: Corvus corone
family: Corvidae
geolocation:
  name: Bois des Éboulures, Franconville, France
  latitude: 48.998249
  longitude: 2.216214
photo: "./E7D23094.jpg"
exif:
  iso: 3200
  aperture: 6.3
  shutterSpeed: "1/800"
  focalLength: 600
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
