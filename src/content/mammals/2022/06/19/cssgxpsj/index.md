---
id: cssgxpsj
slug: photo/cssgxpsj
title: European Rabbit
description: An European Rabbit kitten in Gennevilliers, France.
createdAt: 2022-04-16T10:45:00Z
commonName: European Rabbit
scientificName: Oryctolagus cuniculus
family: Leporidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934845
  longitude: 2.314261
photo: "./E7D21631.jpg"
exif:
  iso: 1000
  aperture: 8
  shutterSpeed: "1/1250"
  focalLength: 361
  camera: Canon EOS 7D Mark II
  lens: Sigma 150-600mm F5-6.3 DG OS HSM | Contemporary
---
