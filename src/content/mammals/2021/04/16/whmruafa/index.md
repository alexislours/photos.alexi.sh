---
id: whmruafa
slug: photo/whmruafa
title: European Rabbit
description: An European Rabbit kitten in Gennevilliers, France.
createdAt: 2021-06-19T06:42:00Z
commonName: European Rabbit
scientificName: Oryctolagus cuniculus
family: Leporidae
geolocation:
  name: Parc des Chanteraines, Gennevilliers, France
  latitude: 48.934845
  longitude: 2.314261
photo: "./E7D23919.jpg"
exif:
  iso: 3200
  aperture: 5.6
  shutterSpeed: "1/3200"
  focalLength: 300
  camera: Canon EOS 7D Mark II
  lens: TAMRON SP 70-300mm F/4-5.6 Di VC USD
---
