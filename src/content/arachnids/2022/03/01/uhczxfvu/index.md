---
id: uhczxfvu
slug: photo/uhczxfvu
title: Segestria bavarica
description: A male Segestria bavarica in Franconville, France.
createdAt: 2022-03-01T21:01:00Z
commonName: Segestria bavarica
scientificName: Segestria bavarica
family: Segestriidae
geolocation:
  name: Franconville, France
  latitude: 48.996989
  longitude: 2.217893
photo: "./E7D23729.jpg"
exif:
  iso: 500
  aperture: 9
  shutterSpeed: "1/100"
  focalLength: 70
  camera: Canon EOS 7D Mark II
  lens: Sigma 70mm F2.8 DG MACRO | Art
---
