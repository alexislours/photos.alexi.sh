---
id: sdgnrtge
slug: photo/sdgnrtge
title: Fork-palped Harvestman
description: A Fork-palped Harvestman in Franconville, France.
createdAt: 2022-09-06T08:21:00Z
commonName: Fork-palped Harvestman
scientificName: Dicranopalpus ramosus
family: Phalangiidae
geolocation:
  name: Franconville, France
  latitude: 48.996989
  longitude: 2.217893
photo: "./759A3310.jpg"
exif:
  iso: 320
  aperture: 6.3
  shutterSpeed: "1/100"
  focalLength: 70
  camera: Canon EOS R7
  lens: Sigma 70mm F2.8 DG MACRO | Art
---
