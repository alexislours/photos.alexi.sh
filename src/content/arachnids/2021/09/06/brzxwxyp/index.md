---
id: brzxwxyp
slug: photo/brzxwxyp
title: House Jumping Spider
description: A House Jumping Spider in Franconville, France.
createdAt: 2021-09-06T08:21:00Z
commonName: House Jumping Spider
scientificName: Pseudeuophrys lanigera
family: Salticidae
geolocation:
  name: Franconville, France
  latitude: 48.996989
  longitude: 2.217893
photo: "./E7D22236.jpg"
exif:
  iso: 200
  aperture: 10
  shutterSpeed: "1/80"
  focalLength: 70
  camera: Canon EOS 7D Mark II
  lens: Sigma 70mm F2.8 DG MACRO | Art
---
