---
id: wtzqhzem
slug: photo/wtzqhzem
title: Silver-sided Sector Spider
description: A Silver-sided Sector Spider in Franconville, France.
createdAt: 2021-09-06T20:39:00Z
commonName: Silver-sided Sector Spider
scientificName: Zygiella x-notata
family: Phonognathidae
geolocation:
  name: Franconville, France
  latitude: 48.996989
  longitude: 2.217893
photo: "./E7D22270.jpg"
exif:
  iso: 200
  aperture: 10
  shutterSpeed: "1/80"
  focalLength: 70
  camera: Canon EOS 7D Mark II
  lens: Sigma 70mm F2.8 DG MACRO | Art
---
