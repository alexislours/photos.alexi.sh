import rss from "@astrojs/rss";
import { getCollection } from "astro:content";
import { SITE_TITLE, SITE_DESCRIPTION } from "../constants";
import type { APIContext } from "astro";

export async function GET(context: APIContext & { site: URL }) {
  const posts = (await getCollection("birds")).sort(
    (a, b) => b.data.createdAt.valueOf() - a.data.createdAt.valueOf()
  );
  return rss({
    title: SITE_TITLE,
    description: SITE_DESCRIPTION,
    site: context.site,
    stylesheet: "/rss/styles.xsl",
    items: posts.map((post) => ({
      ...post.data,
      pubDate: post.data.createdAt,
      content: post.data.description,
      link: `/${post.slug}/`,
    })),
  });
}
