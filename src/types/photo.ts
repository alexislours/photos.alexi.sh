export type Photo = {
  id: string;
  slug: string;
  collection: string;
  data: {
    id: string;
    title: string;
    description: string;
    isLarge?: boolean;
    createdAt: Date;
    photo: {
      src: string;
      width: number;
      height: number;
      format: "jpg";
    };
  };
};
