export type Gallery = {
  id: string;
  slug: string;
  title: string;
  data: {
    id: string;
    title: string;
    description: string;
    cover: {
      src: string;
      width: number;
      height: number;
      format: "jpg";
    };
  };
};
